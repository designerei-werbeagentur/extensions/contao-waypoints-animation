<?php

$GLOBALS['TL_DCA']['tl_content']['fields']['animationType'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                'fadeIn',
                                'fadeInDown',
                                'fadeInDownBig',
                                'fadeInLeft',
                                'fadeInLeftBig',
                                'fadeInRight',
                                'fadeInRightBig',
                                'fadeInUp',
                                'fadeInUpBig'
                              ),
    'eval'                    => array('tl_class'=>'w50', 'includeBlankOption'=>true),
    'sql'                     => "varchar(32) default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['animationDelay'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                'delay-1s',
                                'delay-2s',
                                'delay-3s',
                                'delay-4s',
                                'delay-5s'
                              ),
    'reference'               => ($GLOBALS['TL_LANG']['tl_content']['animationDelay']['options'] ?? null),
    'eval'                    => array('tl_class'=>'w50', 'includeBlankOption'=>true, 'blankOptionLabel'=>'0s'),
    'sql'                     => "varchar(32) default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['animationSpeed'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                'slow',
                                'slower',
                                'fast',
                                'faster'
                              ),
    'reference'               => ($GLOBALS['TL_LANG']['tl_content']['animationSpeed']['options'] ?? null),
    'eval'                    => array('tl_class'=>'w50', 'includeBlankOption'=>true, 'blankOptionLabel'=>'1000ms'),
    'sql'                     => "varchar(32) default ''"
);

// add field to all palettes
foreach ($GLOBALS['TL_DCA']['tl_content']['palettes'] as $key => $palette) {
    if (!is_array($palette) && is_string($palette)) {
        if (strpos($palette, "{template_legend:hide}")) {
            $GLOBALS['TL_DCA']['tl_content']['palettes'][$key] = str_replace('{template_legend:hide}', '{animation_legend},animationType,animationDelay,animationSpeed;{template_legend:hide}', $GLOBALS['TL_DCA']['tl_content']['palettes'][$key]);
        }
    }
}
