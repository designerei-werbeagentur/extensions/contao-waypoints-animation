<?php

namespace designerei\ContaoWaypointsAnimationBundle\ContaoManager;

use designerei\ContaoWaypointsAnimationBundle\ContaoWaypointsAnimationBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoWaypointsAnimationBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
