<?php

namespace designerei\ContaoWaypointsAnimationBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ContaoWaypointsAnimationListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        // extend classes / animationType
        if($template->animationType) {
          $template->class .= ' animate__animated ' . $template->animationType;

          // extend classes / animationDelay & animationSpeed
          if($template->animationDelay) {
            $template->class .= ' animate__' . $template->animationDelay;
          }

          if($template->animationSpeed) {
            $template->class .= ' animate__' . $template->animationSpeed;
          }
        }
    }
}
